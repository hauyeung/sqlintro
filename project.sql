--4
create table category (
category_code number(5) not null primary key,
category_name varchar(3),
constraint chk_category_name check (category_name in ('STA','VEG','FRT','DRK','OTH'))
);

create table item (item_no number(5) primary key,
item_name varchar(200) not null, 
category_code number(5), 
qty_in number(5), 
qty_out number(5), 
qty_bal number(5,2) not null, 
last_purchase_date date, 
last_sales_date date, 
cost_price number(7,2), 
sales_price number(7,2),
constraint category_code foreign key(category_code) references category(category_code));


create table invoice(
inv_no number(5) primary key,
invdate date default(sysdate),
item_no number(5),
qty number(5),
sales_price number(7,2),
discount number(3,2),
total_price number(7,2),
constraint item_no foreign key (item_no) references item(item_no)
);
--5
create sequence item_seq start with 100 increment by 1 nocache nocycle;
create sequence inv_seq start with 1 increment by 1 nocache nocycle;

create sequence category_seq;

insert into category values (category_seq.nextval,'STA');
insert into category values (category_seq.nextval,'VEG');
insert into category values (category_seq.nextval,'FRT');
insert into category values (category_seq.nextval,'DRK');
insert into category values (category_seq.nextval,'OTH');

--6
insert into item values (item_seq.nextval,
'cabbage',
2,
10,
8,
500.00,
TO_DATE('2014/07/09', 'yyyy/mm/dd'),
TO_DATE('2014/08/09', 'yyyy/mm/dd'),
10,
11
);

insert into item values (item_seq.nextval,
'lettuce',
2,
10,
8,
690.00,
TO_DATE('2014/07/09', 'yyyy/mm/dd'),
TO_DATE('2014/08/09', 'yyyy/mm/dd'),
10,
11
);

insert into item values (item_seq.nextval,
'apple',
3,
10,
8,
500.00,
TO_DATE('2014/07/09', 'yyyy/mm/dd'),
TO_DATE('2014/08/09', 'yyyy/mm/dd'),
10,
11
);

insert into item values (item_seq.nextval,
'cabbage',
2,
10,
8,
480.00,
TO_DATE('2014/07/09', 'yyyy/mm/dd'),
TO_DATE('2014/08/09', 'yyyy/mm/dd'),
10,
11
);

insert into item values (item_seq.nextval,
'orange',
3,
10,
8,
980.00,
TO_DATE('2014/07/09', 'yyyy/mm/dd'),
TO_DATE('2014/08/09', 'yyyy/mm/dd'),
10,
11
);

insert into item values (item_seq.nextval,
'pencil',
1,
10,
8,
300.00,
TO_DATE('2014/07/09', 'yyyy/mm/dd'),
TO_DATE('2014/08/09', 'yyyy/mm/dd'),
10,
11
);

insert into item values (item_seq.nextval,
'colored pencil',
1,
10,
8,
230.00,
TO_DATE('2014/04/19', 'yyyy/mm/dd'),
TO_DATE('2014/08/29', 'yyyy/mm/dd'),
10,
11
);

insert into item values (item_seq.nextval,
'pen',
1,
10,
8,
200.00,
TO_DATE('2014/03/09', 'yyyy/mm/dd'),
TO_DATE('2014/05/10', 'yyyy/mm/dd'),
10,
11
);

insert into item values (item_seq.nextval,
'watermelon',
3,
200,
80,
300.00,
TO_DATE('2014/07/01', 'yyyy/mm/dd'),
TO_DATE('2014/08/02', 'yyyy/mm/dd'),
10,
11
);

insert into item values (item_seq.nextval,
'eraser',
1,
580,
287,
300.00,
TO_DATE('2014/07/09', 'yyyy/mm/dd'),
TO_DATE('2014/12/02', 'yyyy/mm/dd'),
10,
11
);

insert into invoice (inv_no, invdate, item_no, qty, discount) values (inv_seq.nextval,TO_DATE('2014/07/09', 'yyyy/mm/dd'),101, 5,0);
update invoice set sales_price = (select sales_price from item where item_no = 101) where inv_no = 1;
update invoice set total_price = (select sales_price*qty from invoice where inv_no = 1);

insert into invoice (inv_no, invdate, item_no, qty, discount) values (inv_seq.nextval,TO_DATE('2014/07/10', 'yyyy/mm/dd'),102, 8,0);
update invoice set sales_price = (select sales_price from item where item_no = 102) where inv_no = 2;
update invoice set total_price = (select sales_price*qty from invoice where inv_no = 2);

insert into invoice (inv_no, invdate, item_no, qty, discount) values (inv_seq.nextval,TO_DATE('2014/07/10', 'yyyy/mm/dd'),103,18,0);
update invoice set sales_price = (select sales_price from item where item_no = 103) where inv_no = 3;
update invoice set total_price = (select sales_price*qty from invoice where inv_no = 3);

insert into invoice (inv_no, invdate, item_no, qty, discount) values (inv_seq.nextval,TO_DATE('2014/07/11', 'yyyy/mm/dd'),104,2,0);
update invoice set sales_price = (select sales_price from item where item_no = 104) where inv_no = 4;
update invoice set total_price = (select sales_price*qty from invoice where inv_no = 4);

insert into invoice (inv_no, invdate, item_no, qty, discount) values (inv_seq.nextval,TO_DATE('2014/07/12', 'yyyy/mm/dd'),105,2,0);
update invoice set sales_price = (select sales_price from item where item_no = 105) where inv_no = 5;
update invoice set total_price = (select sales_price*qty from invoice where inv_no = 5);

--7a
alter table item add (qty_min number check (qty_min > -1));

--7b
alter table item modify (item_name varchar(205));

--7c
update item set qty_min = 1 where item_no in (Select Rownum r From dual Connect By Rownum <= 112);

--8a
select category.category_code, category.category_name, item.item_no, item.item_name, item.sales_price, item.qty_bal from category inner join item on category.category_code = item.category_code order by category.category_name, item.item_no;
--8b
select item_no, item_name, last_purchase_date, last_sales_date from item where (qty_bal < qty_min) or (last_sales_date - last_purchase_date >= 10);
--8c
select * from invoice where qty > 10 and sales_price >= 5;
--8d
select sum(item.sales_price), min(item.sales_price), max(item.sales_price) from item inner join category on item.category_code = category.CATEGORY_CODE group by item.sales_price;
--8e
select inv_no, invdate, qty as qtysold, sales_price, total_price from invoice where ((select months_between(sysdate, invdate) from dual) = 2);
--8f
select item_no, item_name, cost_price, sales_price from item where(sysdate - last_purchase_date > 50);

--9
create view item_view as 
  select * from item where qty_bal < qty_min order by item_name;
  
--10  
create index item_index on item(item_name);

--11
delete from invoice where ((sysdate - INVDATE) >90);