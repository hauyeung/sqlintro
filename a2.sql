insert into orders (order#, customer#, orderdate) values (1021, 1009, '20-JUL-2009') 

select * from orders

update orders set shipzip = 33222 where order# = 1017

insert into orders (order#, customer#) values (1023, 1009) 

define isbn = 1059831198
define cos = 20
update books set cost = &cos where isbn = &isbn

select * from books

rollback

alter table orders disable constraint orders_customer#_fk;
alter table orderitems disable constraint ORDERITEMS_ORDER#_FK;
delete from orders where order# =1005

select * from orders
rollback

create sequence customerseq increment by 1 nocache nocycle

select customerseq.nextval from dual

insert into customers (customer#, lastname, firstname, zip) values (customerseq.nextval, 'Shoulders', 'Frank', 23567)
select * from customers

create sequence MY_FIRST_SEQ increment by -3 start with 5 maxvalue 5 minvalue 0 nocache nocycle;


select my_first_seq.nextval from dual;

alter sequence my_first_seq minvalue -1000

select user from dual;

create or replace synonym NUMGEN for MY_FIRST_SEQ;

select numgen.nextval from dual

select numgen.currval from dual;

drop synonym NUMGEN

create bitmap index state_ix on customers (state)

select dbms_metadata.get_ddl('INDEX','state_ix','sys') from dual;