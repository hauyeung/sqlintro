describe books
select title , pubdate from books

describe customers
select customer#, city, state from customers

describe publisher
select name, contact, phone from publisher

select category from books

describe orders
select customer# from orders

select title, category from books

describe author
select fname, lname from author

describe orderitems
select order#, item#, isbn, quantity, paideach, quantity*paideach as "Item Total" from orderitems

CREATE TABLE CATEGORY (CatCode varchar(2), CatDesc varchar(10))

CREATE TABLE EMPLOYEES (Emp# number(5), Lastname varchar(100), Firstname varchar(100), Job_class varchar2(4))

ALTER TABLE EMPLOYEES ADD (EmpDate date default SYSTIMESTAMP, EndDate date)

ALTER TABLE EMPLOYEES MODIFY (Job_class varchar(2))

ALTER TABLE EMPLOYEES RENAME TO JL_EMPS

describe jl_emps

CREATE TABLE store_reps
(rep_ID NUMBER(5) not null,
last VARCHAR2(15),
first VARCHAR2(10),
comm CHAR(1), primary key (rep_ID) );

describe store_reps

ALTER TABLE store_reps MODIFY (last not null, first not null)

ALTER TABLE store_reps add constraint ck_comm check (comm in ('y','n'))

ALTER TABLE store_reps ADD (Base_salary NUMBER(7,2) check (Base_salary > 0))

CREATE TABLE BOOK_STORES (Store_ID number(8), "Name" varchar2(30) unique not null, Contact varchar2(30), Rep_ID varchar2(5))

describe book_stores